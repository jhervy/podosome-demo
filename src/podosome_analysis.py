# generic import
import math
import os
import numpy as np
import pandas as pd
import csv
import re
import yaml
from scipy.optimize import curve_fit

###################################
### Config parameters #############
###################################
config_yaml = open('config.yaml')
config_dict = yaml.load(config_yaml, Loader=yaml.FullLoader)
__RIGIDITY_ACTIN_FILAMENT__ = float(config_dict['Actin']['rigidity'])

###################################
### Extract data ##################
###################################

def get_data(filename):
    file = open(filename,'r')
    return np.array([line.split(',') for line in file.readlines()])

def get_point_coordinates(data):
    x_position = [float(i) for i in data[:,1]]
    y_position = [float(i) for i in data[:,2]]
    z_position = [float(i) for i in data[:,3]]
    return x_position, y_position, z_position;

def get_filament_length(data):
    return [float(i) for i in data[:,10]]

def get_filament_number(data):
    return [float(i) for i in data[:,4]]

##sum of local energy
def get_local_energy(data):
    return [float(i) for i in data[:,103]]

##local energy (column 91 in documentation)
def get_loc_energy(data):
    return [float(i) for i in data[:,90]]

def get_point_number_filament(data):
    return [float(i) for i in data[:,5]]

def get_mean_local_curvature(data):
    return [float(i) for i in data[:,105]]

##Extract core coordinates from the filename
def get_core_coordinates(filename):
    find_x = re.search('xCoeur(\d+\.\d+)',filename, re.IGNORECASE)
    find_y = re.search('yCoeur(\d+\.\d+)',filename, re.IGNORECASE)
    return float(find_x.group(1)), float(find_y.group(1))

def correct_z_position(z_position = None, tag = None, shift = None, **kwargs):
    if tag == 'notinverted':
        z_membrane = min(z_position) + shift
        z_corrected = [(z - z_membrane) for z in z_position]
    elif tag == 'inverted':
        z_membrane = max(z_position) - shift
        z_corrected = [z_membrane - z for z in z_position]
    return z_corrected

### Get number of points within the distance range (10nm from the membrane)
def get_number_pts_close_membrane(data):
    return [float(i) for i in data[:,121]]

def get_radial_distance_all_filament(x_position, y_position, x_core, y_core):
    return [math.sqrt((x_position[index] - x_core)**2 + (y_position[index] - y_core)**2) for index in range(len(x_position))]

### Get radial distance from the core
def get_radial_distance(data, filename):
    x_core, y_core = get_core_coordinates(filename)
    x_position, y_position, z_position = get_point_coordinates(data)
    return [math.sqrt((x_position[i] - x_core)**2 + (y_position[i] - y_core)**2) for i in range(len(x_position))]

###################################
### Point coordinates #############
###################################

### Get point coordinates for the number_target-th filament
def get_point_coordinates_single_filament(x_position, y_position, z_position, filament_number, point_number_filament, number_target):
    x_position_single_filament = []
    y_position_single_filament = []
    z_position_single_filament = []
    point_number_single_filament = []
    for i in range(len(filament_number)):
        if filament_number[i] == number_target:
            x_position_single_filament.append(x_position[i])
            y_position_single_filament.append(y_position[i])
            z_position_single_filament.append(z_position[i])
            point_number_single_filament.append(point_number_filament[i])
    return x_position_single_filament, y_position_single_filament, z_position_single_filament, point_number_single_filament;

### Get point coordinates for the number_target-th filament
### Function adapted from 'get_point_coordinates_single_filament' for filaments close to the membrane
def get_point_coordinates_single_filament_membrane(x_position, y_position, z_position, filament_number, point_number_filament, nb_pts_distance_range, number_target):
    x_position_single_filament = []
    y_position_single_filament = []
    z_position_single_filament = []
    point_number_single_filament = []
    nb_pts_distance_range_single_filament = []
    for i in range(len(filament_number)):
        if filament_number[i] == number_target:
            x_position_single_filament.append(x_position[i])
            y_position_single_filament.append(y_position[i])
            z_position_single_filament.append(z_position[i])
            point_number_single_filament.append(point_number_filament[i])
            nb_pts_distance_range_single_filament.append(nb_pts_distance_range[i])
    return x_position_single_filament, y_position_single_filament, z_position_single_filament, point_number_single_filament, nb_pts_distance_range_single_filament;

def get_all_parameters_single_filament(filename, number_target):
    ## get parameters for all filaments
    data = get_data(filename)
    x_position, y_position, z_position = get_point_coordinates(data)
    point_number_filament = get_point_number_filament(data)
    filament_number = get_filament_number(data)
    filament_length = get_filament_length(data)
    ## initializing parameters for the 'number target'-th single filament
    x_position_single_filament = []
    y_position_single_filament = []
    z_position_single_filament = []
    point_number_single_filament = []
    filament_length_single_filament = []
    for index in range(len(filament_number)):
        if filament_number[index] == number_target:
            x_position_single_filament.append(x_position[index])
            y_position_single_filament.append(y_position[index])
            z_position_single_filament.append(z_position[index])
            point_number_single_filament.append(point_number_filament[index])
            filament_length_single_filament.append(filament_length[index])
    return x_position_single_filament, y_position_single_filament, z_position_single_filament, point_number_single_filament, filament_length_single_filament;

###################################
### Filament length ###############
###################################

def get_parameters_for_length(filament_number, point_number_filament, filament_length, radial_distance, number_target):
    ## initializing parameters for the 'number target'-th single filament
    point_number_single_filament = []
    filament_length_single_filament = []
    radial_distance_single_filament = []
    for index in range(len(filament_number)):
        if filament_number[index] == number_target:
            point_number_single_filament.append(point_number_filament[index])
            filament_length_single_filament.append(filament_length[index])
            radial_distance_single_filament.append(radial_distance[index])
    return point_number_single_filament, filament_length_single_filament,radial_distance_single_filament;

def get_length_single_filament(point_number_single_filament, filament_length_single_filament,radial_distance_single_filament,radius,delta):
    length_weighted_inside = interval(filament_length_single_filament,radial_distance_single_filament,radius,delta)
    return remove_string_list(length_weighted_inside)

def get_length_all_filaments(filament_number, point_number_filament, filament_length, radial_distance, radius, delta):
    filament_length_all_filaments_method_weighted = []
    for number_target in range(int(max(filament_number))):
        point_number_single_filament, filament_length_single_filament,radial_distance_single_filament = get_parameters_for_length(filament_number, point_number_filament, filament_length, radial_distance, number_target)
        length_inside_single_filament = get_length_single_filament(point_number_single_filament, filament_length_single_filament,radial_distance_single_filament,radius,delta)
        if len(length_inside_single_filament)>0:
            filament_length_all_filaments_method_weighted.append(length_inside_single_filament)
    return get_joined_list(filament_length_all_filaments_method_weighted)

def get_average_filament_length(filament_number, point_number_filament, filament_length, radial_distance, Radius_lst, delta):
    avg_lst = []
    std_lst = []
    sem_lst = []
    len_interval_lst = []
    for index in range(len(Radius_lst)):
        filament_length_all_filaments_method_weighted = get_length_all_filaments(filament_number, point_number_filament, filament_length, radial_distance, Radius_lst[index], delta)
        avg_value, std_value, sem_value, len_interval = basic_stat(filament_length_all_filaments_method_weighted)
        avg_lst.append(avg_value)
        std_lst.append(std_value)
        sem_lst.append(sem_value)
        len_interval_lst.append(len_interval)
    return avg_lst,std_lst,sem_lst,len_interval_lst;

def get_filament_length_height(x_position, y_position, z_position, filament_number, point_number_filament, filament_length, filename, number_target):
    x_position_single_filament, y_position_single_filament, z_position_single_filament, point_number_single_filament, filament_length_single_filament = get_parameters_for_compression_single_filament(x_position, y_position, z_position, filament_number, point_number_filament,filament_length, number_target)
    len_single_filament = filament_length_single_filament[0] #Filament length
    ## Get point coordinates of the filament center
    n_points = len(x_position_single_filament)
    if (n_points % 2) == 0:
        index_center_filament = math.ceil(n_points/2) - 1
        x_position_center_filament = (x_position_single_filament[index_center_filament]+x_position_single_filament[index_center_filament+1])/2
        y_position_center_filament = (y_position_single_filament[index_center_filament]+y_position_single_filament[index_center_filament+1])/2
        z_position_center_filament = (z_position_single_filament[index_center_filament]+z_position_single_filament[index_center_filament+1])/2
    else:
        index_center_filament = math.ceil(n_points/2) - 1
        x_position_center_filament = x_position_single_filament[index_center_filament]
        y_position_center_filament = y_position_single_filament[index_center_filament]
        z_position_center_filament = z_position_single_filament[index_center_filament]
    ## Get radial distance between the core and the filament center
    x_core, y_core = get_core_coordinates(filename)
    radial_distance_single_filament = (math.sqrt((x_position_center_filament - x_core)**2 + (y_position_center_filament - y_core)**2))
    return radial_distance_single_filament, len_single_filament, z_position_center_filament

def get_length_all_filaments_height(x_position, y_position, z_position, filament_number, point_number_filament, filament_length, filename):
    filament_length_all = []
    radial_distance_all = []
    filament_height_all = []
    for number_target in range(int(max(filament_number))):
        radial_distance_single_filament, len_single_filament, z_position_center_filament = get_filament_length_height(x_position, y_position, z_position, filament_number, point_number_filament, filament_length, filename, number_target)
        filament_length_all.append(len_single_filament)
        radial_distance_all.append(radial_distance_single_filament)
        filament_height_all.append(z_position_center_filament)
    return filament_length_all, radial_distance_all, filament_height_all

def get_length_all_filaments_height_radial_selection(radial_distance_all, filament_length_all, filament_height_all, radius,delta):
    filament_length_inside = interval(filament_length_all,radial_distance_all,radius,delta)
    filament_height_inside = interval(filament_height_all,radial_distance_all,radius,delta)
    return remove_string_list(filament_length_inside),remove_string_list(filament_height_inside);


### Get the list of average filament length as a function of height
def get_list_average_filament_length_height(filament_length,height,bin_width):
    avg_length_lst = []
    std_length_lst = []
    avg_height_lst = []
    percent_lst = []
    initial_value = min(height)
    while initial_value < max(height):
        avg_length, std_length, sem_length, len_interval = basic_stat(remove_string_list(interval(filament_length,height,initial_value,bin_width)))
        avg_length_lst.append(avg_length)
        std_length_lst.append(std_length)
        avg_height_lst.append(initial_value + bin_width/2)
        percent_lst.append( ((initial_value + bin_width/2)-min(height))/(max(height)-min(height)))
        initial_value = initial_value + bin_width
    return avg_length_lst, std_length_lst, avg_height_lst, percent_lst

### Get the list of average filament length as a function of height
def get_list_average_filament_length_height_corrected(filament_length, filament_height, Height_list, delta):
    avg_length_lst = []
    std_length_lst = []
    for height_value in Height_list:
        Length_interval = remove_string_list(interval(filament_length,filament_height,height_value,delta))
        if len(Length_interval)==0:
            avg_length_lst.append(0)
            std_length_lst.append(0)
        else:
            avg_length, std_length, sem_length, len_interval = basic_stat(Length_interval)
            avg_length_lst.append(avg_length)
            std_length_lst.append(std_length)
    return avg_length_lst, std_length_lst

###################################
### Filament energy ###############
###################################

### Functions to get the filament curvature
def get_parameters_for_energy_method_weighted(filament_number,radial_distance, energy, number_target):
    ## initializing parameters for the 'number target'-th single filament
    radial_distance_single_filament = []
    energy_single_filament = []
    for index in range(len(filament_number)):
        if filament_number[index] == number_target:
            radial_distance_single_filament.append(radial_distance[index])
            energy_single_filament.append(energy[index])
    return radial_distance_single_filament, energy_single_filament;

def get_energy_method_weighted_single_filament(energy_single_filament,radial_distance_single_filament,radius,delta):
    energy_weighted_inside = interval(energy_single_filament,radial_distance_single_filament,radius,delta)
    return remove_string_list(energy_weighted_inside)

def get_energy_method_weighted_all_filaments(filament_number,energy,radial_distance,radius,delta):
    energy_all_filaments_method_weighted = []
    for number_target in range(int(max(filament_number))):
        radial_distance_single_filament, energy_single_filament = get_parameters_for_energy_method_weighted(filament_number,radial_distance, energy, number_target)
        energy_inside_single_filament = get_energy_method_weighted_single_filament(energy_single_filament,radial_distance_single_filament,radius,delta)
        if len(energy_inside_single_filament)>0:
            energy_all_filaments_method_weighted.append(energy_inside_single_filament)
    return get_joined_list(energy_all_filaments_method_weighted)

def get_average_energy_method_weighted(filament_number, energy, radial_distance,Radius_lst,delta):
    avg_lst = []
    std_lst = []
    sem_lst = []
    len_interval_lst = []
    for index in range(len(Radius_lst)):
        energy_all_filaments_method_weighted = get_energy_method_weighted_all_filaments(filament_number,energy, radial_distance, Radius_lst[index], delta)
        avg_value, std_value, sem_value, len_interval = basic_stat(energy_all_filaments_method_weighted)
        avg_lst.append(avg_value)
        std_lst.append(std_value)
        sem_lst.append(sem_value)
        len_interval_lst.append(len_interval)
    return avg_lst,std_lst,sem_lst,len_interval_lst;

###################################
### Filament curvature ############
###################################

### Functions to get the filament curvature
def get_parameters_for_curvature_method_weighted(filament_number,radial_distance, curvature, number_target):
    ## initializing parameters for the 'number target'-th single filament
    radial_distance_single_filament = []
    curvature_single_filament = []
    for index in range(len(filament_number)):
        if filament_number[index] == number_target:
            radial_distance_single_filament.append(radial_distance[index])
            curvature_single_filament.append(curvature[index])
    return radial_distance_single_filament, curvature_single_filament;

def get_curvature_method_weighted_single_filament(curvature_single_filament,radial_distance_single_filament,radius,delta):
    curvature_weighted_inside = interval(curvature_single_filament,radial_distance_single_filament,radius,delta)
    return remove_string_list(curvature_weighted_inside)

def get_curvature_method_weighted_all_filaments(filament_number,curvature,radial_distance,radius,delta):
    curvature_all_filaments_method_weighted = []
    for number_target in range(int(max(filament_number))):
        radial_distance_single_filament, curvature_single_filament = get_parameters_for_curvature_method_weighted(filament_number,radial_distance, curvature, number_target)
        curvature_inside_single_filament = get_curvature_method_weighted_single_filament(curvature_single_filament,radial_distance_single_filament,radius,delta)
        if len(curvature_inside_single_filament)>0:
            curvature_all_filaments_method_weighted.append(curvature_inside_single_filament)
    return get_joined_list(curvature_all_filaments_method_weighted)

def get_average_curvature_method_weighted(filament_number, curvature, radial_distance,Radius_lst,delta):
    avg_lst = []
    std_lst = []
    sem_lst = []
    len_interval_lst = []
    for index in range(len(Radius_lst)):
        curvature_all_filaments_method_weighted = get_curvature_method_weighted_all_filaments(filament_number, curvature, radial_distance, Radius_lst[index], delta)
        avg_value, std_value, sem_value, len_interval = basic_stat(curvature_all_filaments_method_weighted)
        avg_lst.append(avg_value)
        std_lst.append(std_value)
        sem_lst.append(sem_value)
        len_interval_lst.append(len_interval)
    return avg_lst,std_lst,sem_lst,len_interval_lst;

###################################
### Filament compression ##########
###################################

def get_parameters_for_compression_single_filament(x_position, y_position, z_position, filament_number, point_number_filament, filament_length, number_target):
    x_position_single_filament = []
    y_position_single_filament = []
    z_position_single_filament = []
    point_number_single_filament = []
    filament_length_single_filament = []
    for i in range(len(filament_number)):
        if filament_number[i] == number_target:
            x_position_single_filament.append(x_position[i])
            y_position_single_filament.append(y_position[i])
            z_position_single_filament.append(z_position[i])
            point_number_single_filament.append(point_number_filament[i])
            filament_length_single_filament.append(filament_length[i])
    return x_position_single_filament, y_position_single_filament, z_position_single_filament, point_number_single_filament, filament_length_single_filament;

def get_compression_single_filament(x_position, y_position, z_position, filament_number, point_number_filament, filament_length, filename, number_target):
    x_position_single_filament, y_position_single_filament, z_position_single_filament, point_number_single_filament, filament_length_single_filament = get_parameters_for_compression_single_filament(x_position, y_position, z_position, filament_number, point_number_filament,filament_length, number_target)
    ## Get point coordinates of the 1st and last point
    x_position_single_filament_1st_point = x_position_single_filament[0]
    x_position_single_filament_last_point = x_position_single_filament[len(x_position_single_filament)-1]
    y_position_single_filament_1st_point = y_position_single_filament[0]
    y_position_single_filament_last_point = y_position_single_filament[len(y_position_single_filament)-1]
    z_position_single_filament_1st_point = z_position_single_filament[0]
    z_position_single_filament_last_point = z_position_single_filament[len(z_position_single_filament)-1]
    ## Get the chord and the compression value for a single filament
    chord = get_norm_vector(x_position_single_filament_1st_point,y_position_single_filament_1st_point,z_position_single_filament_1st_point,x_position_single_filament_last_point,y_position_single_filament_last_point,z_position_single_filament_last_point)
    len_filament = filament_length_single_filament[0]
    compression_value = 1 - (chord/len_filament)
    ## Get point coordinates of the filament center
    n_points = len(x_position_single_filament)
    if (n_points % 2) == 0:
        index_center_filament = math.ceil(n_points/2) - 1
        x_position_center_filament = (x_position_single_filament[index_center_filament]+x_position_single_filament[index_center_filament+1])/2
        y_position_center_filament = (y_position_single_filament[index_center_filament]+y_position_single_filament[index_center_filament+1])/2
    else:
        index_center_filament = math.ceil(n_points/2) - 1
        x_position_center_filament = x_position_single_filament[index_center_filament]
        y_position_center_filament = y_position_single_filament[index_center_filament]
    ## Get radial distance between the core and the filament center
    x_core, y_core = get_core_coordinates(filename)
    radial_distance_single_filament = (math.sqrt((x_position_center_filament - x_core)**2 + (y_position_center_filament - y_core)**2))
    #return compression_value, radial_distance_single_filament, filament_length_single_filament[0]
    return compression_value, radial_distance_single_filament, len_filament

def get_parameters_for_compression_single_filament_weighted_method(x_position, y_position, z_position, filament_number, filament_length, radial_distance, number_target):
    x_position_single_filament = []
    y_position_single_filament = []
    z_position_single_filament = []
    filament_length_single_filament = []
    radial_distance_single_filament = []
    compression_single_filament = []
    for index in range(len(filament_number)):
        if filament_number[index] == number_target:
            x_position_single_filament.append(x_position[index])
            y_position_single_filament.append(y_position[index])
            z_position_single_filament.append(z_position[index])
            filament_length_single_filament.append(filament_length[index])
            radial_distance_single_filament.append(radial_distance[index])
    ## Get point coordinates of the 1st and last point
    x_position_single_filament_1st_point = x_position_single_filament[0]
    x_position_single_filament_last_point = x_position_single_filament[len(x_position_single_filament)-1]
    y_position_single_filament_1st_point = y_position_single_filament[0]
    y_position_single_filament_last_point = y_position_single_filament[len(y_position_single_filament)-1]
    z_position_single_filament_1st_point = z_position_single_filament[0]
    z_position_single_filament_last_point = z_position_single_filament[len(z_position_single_filament)-1]
    ## Get the chord and the compression value for a single filament
    chord = get_norm_vector(x_position_single_filament_1st_point,y_position_single_filament_1st_point,z_position_single_filament_1st_point,x_position_single_filament_last_point,y_position_single_filament_last_point,z_position_single_filament_last_point)
    len_filament = filament_length_single_filament[0]
    compression_value = 1 - (chord/len_filament)
    curvature_predict = len_filament / (np.sqrt(24*compression_value))
    compression_single_filament = [compression_value for i in range(len(x_position_single_filament))]
    curvature_predict_single_filament = [curvature_predict for i in range(len(x_position_single_filament))]
    return radial_distance_single_filament, compression_single_filament, curvature_predict_single_filament, filament_length_single_filament;

def get_compression_single_filament_weighted_method(radial_distance_single_filament, compression_single_filament, curvature_predict_single_filament, filament_length_single_filament,radius,delta):
    compression_weighted_inside = interval(compression_single_filament,radial_distance_single_filament,radius,delta)
    length_weighted_inside = interval(filament_length_single_filament,radial_distance_single_filament,radius,delta)
    curvature_predict_weighted_inside = interval(curvature_predict_single_filament,radial_distance_single_filament,radius,delta)
    return remove_string_list(compression_weighted_inside),remove_string_list(length_weighted_inside),remove_string_list(curvature_predict_weighted_inside);

def get_compression_method_weighted_all_filaments(x_position, y_position, z_position, filament_number, filament_length, radial_distance, radius, delta):
    compression_all_filaments_method_weighted = []
    length_all_filaments_method_weighted = []
    curvature_predict_all_filaments_method_weighted = []
    for number_target in range(int(max(filament_number))):
        radial_distance_single_filament, compression_single_filament, curvature_predict_single_filament, filament_length_single_filament = get_parameters_for_compression_single_filament_weighted_method(x_position, y_position, z_position, filament_number, filament_length, radial_distance, number_target)
        compression_inside, length_inside, curvature_predict_inside = get_compression_single_filament_weighted_method(radial_distance_single_filament, compression_single_filament, curvature_predict_single_filament, filament_length_single_filament,radius,delta)
        if len(compression_inside)>0:
            compression_all_filaments_method_weighted.append(compression_inside)
            length_all_filaments_method_weighted.append(length_inside)
            curvature_predict_all_filaments_method_weighted.append(curvature_predict_inside)
    return get_joined_list(compression_all_filaments_method_weighted), get_joined_list(length_all_filaments_method_weighted), get_joined_list(curvature_predict_all_filaments_method_weighted)

def get_average_compression_method_weighted(x_position, y_position, z_position, filament_number, filament_length, radial_distance, Radius_lst, delta):
    avg_lst_compression = []
    std_lst_compression = []
    sem_lst_compression = []
    len_interval_lst_compression = []
    avg_lst_length = []
    std_lst_length = []
    sem_lst_length = []
    len_interval_lst_length = []
    avg_lst_curvature_predict = []
    std_lst_curvature_predict = []
    sem_lst_curvature_predict = []
    len_interval_lst_curvature_predict = []
    for index in range(len(Radius_lst)):
        compression,length,curvature = get_compression_method_weighted_all_filaments(x_position, y_position, z_position, filament_number, filament_length, radial_distance, Radius_lst[index], delta)
        avg_value_compression, std_value_compression, sem_value_compression, len_interval_compression = basic_stat(compression)
        avg_lst_compression.append(avg_value_compression)
        std_lst_compression.append(std_value_compression)
        sem_lst_compression.append(sem_value_compression)
        len_interval_lst_compression.append(len_interval_compression)
        avg_value_length, std_value_length, sem_value_length, len_interval_length = basic_stat(length)
        avg_lst_length.append(avg_value_length)
        std_lst_length.append(std_value_length)
        sem_lst_length.append(sem_value_length)
        len_interval_lst_length.append(len_interval_length)
        avg_value_curvature, std_value_curvature, sem_value_curvature, len_interval_curvature = basic_stat(curvature)
        avg_lst_curvature_predict.append(avg_value_curvature)
        std_lst_curvature_predict.append(std_value_curvature)
        sem_lst_curvature_predict.append(sem_value_curvature)
        len_interval_lst_curvature_predict.append(len_interval_curvature)
    return avg_lst_compression,std_lst_compression,sem_lst_compression,len_interval_lst_compression,avg_lst_length,std_lst_length,sem_lst_length,len_interval_lst_length,avg_lst_curvature_predict,std_lst_curvature_predict,sem_lst_curvature_predict,len_interval_lst_curvature_predict;

### get list of filament compression for all filaments
def get_compression_all_filaments(x_position, y_position, z_position, filament_number, point_number_filament, filament_length, filename):
    compression_all_filaments = []
    radial_distance_all_filaments = []
    filament_length_all_filaments = []
    for number_target in range(int(max(filament_number))):
        compression, radial_distance, filament_length_single = get_compression_single_filament(x_position, y_position, z_position, filament_number, point_number_filament, filament_length, filename, number_target)
        compression_all_filaments.append(compression)
        radial_distance_all_filaments.append(radial_distance)
        filament_length_all_filaments.append(filament_length_single)
    return compression_all_filaments, radial_distance_all_filaments, filament_length_all_filaments

### Get list of compression for all filaments inside the interval : radius < radial_distance < radius + delta
def get_compression_all_filaments_inside_interval(compression_all_filaments,radial_distance_all_filaments,filament_length_all_filaments,radius,delta):
    compression_all_filaments_inside_interval = interval(compression_all_filaments,radial_distance_all_filaments,radius,delta)
    length_all_filaments_inside_interval = interval(filament_length_all_filaments,radial_distance_all_filaments,radius,delta)
    return remove_string_list(compression_all_filaments_inside_interval),remove_string_list(length_all_filaments_inside_interval)

### Get the list of average filament compression
def get_list_average_filament_compression(compression_all_filaments,radial_distance_all_filaments,filament_length_all_filaments,Radius_lst,delta):
    avg_compression_lst = []
    std_compression_lst = []
    sem_compression_lst = []
    len_interval_compression_lst = []
    avg_length_lst = []
    std_length_lst = []
    sem_length_lst = []
    len_interval_length_lst = []
    for radius in Radius_lst:
        compression_lst, length_lst = get_compression_all_filaments_inside_interval(compression_all_filaments,radial_distance_all_filaments,filament_length_all_filaments,radius,delta)
        ## statistics for the filament compression
        avg_value_comp, std_value_comp, sem_value_comp, len_interval_comp = basic_stat(compression_lst)
        avg_compression_lst.append(avg_value_comp)
        std_compression_lst.append(std_value_comp)
        sem_compression_lst.append(sem_value_comp)
        len_interval_compression_lst.append(len_interval_comp)
        ## statistics for the filament length
        avg_value_length, std_value_length, sem_value_length, len_interval_length = basic_stat(length_lst)
        #avg_length_lst.append(avg_value_length)
        avg_length_lst.append(np.average(length_lst))
        #test something else here like avg_length_lst.append(np.average(length_lst))
        std_length_lst.append(std_value_length)
        sem_length_lst.append(sem_value_length)
        len_interval_length_lst.append(len_interval_length)
    return avg_compression_lst, std_compression_lst, sem_compression_lst, len_interval_compression_lst, avg_length_lst, std_length_lst, sem_length_lst, len_interval_length_lst


###################################
### Filament orientation ##########
###################################

### get norm vector from the cartesian coordinates of 2 points
def get_norm_vector(x_position_i,y_position_i,z_position_i,x_position_f,y_position_f,z_position_f):
    return  np.sqrt((x_position_f - x_position_i)**2 + (y_position_f - y_position_i)**2 + (z_position_f - z_position_i)**2)

### get angle (elevation) from the cartesian coordinates of 2 points
def orientation(x_position_i,y_position_i,z_position_i,x_position_f,y_position_f,z_position_f):
    norm_vector = get_norm_vector(x_position_i,y_position_i,z_position_i,x_position_f,y_position_f,z_position_f)
    #return 90 - (180/(np.pi))*np.arccos(np.abs(z_position_f - z_position_i)/norm_vector)
    return (180/(np.pi))*np.abs(np.arcsin((z_position_f - z_position_i)/norm_vector))

### get list of filament orientations for a single filament
def get_orientation_single_filament(x_position, y_position, z_position, filament_number, point_number_filament, filename, number_target):
    x_position_single_filament,y_position_single_filament,z_position_single_filament,point_number_single_filament = get_point_coordinates_single_filament(x_position, y_position, z_position, filament_number, point_number_filament, number_target)
    x_core, y_core = get_core_coordinates(filename)
    orientation_single_filament = []
    height_single_filament = []
    radial_distance_single_filament = []
    for i in range(len(point_number_single_filament)-1):
        orientation_single_filament.append(orientation(x_position_single_filament[i],y_position_single_filament[i],z_position_single_filament[i],x_position_single_filament[i+1],y_position_single_filament[i+1],z_position_single_filament[i+1]))
        x_position_center_segment = (x_position_single_filament[i] + x_position_single_filament[i+1])/2
        y_position_center_segment = (y_position_single_filament[i] + y_position_single_filament[i+1])/2
        z_position_center_segment = (z_position_single_filament[i] + z_position_single_filament[i+1])/2
        height_single_filament.append(z_position_center_segment)
        radial_distance_single_filament.append(math.sqrt((x_position_center_segment - x_core)**2 + (y_position_center_segment - y_core)**2))
    return orientation_single_filament, height_single_filament, radial_distance_single_filament

## Simplification of the function get_orientation_single_filament()
def get_orientation_single_filament_simple(x_position, y_position, z_position, filament_number, point_number_filament, filename, number_target):
    x_position_single_filament,y_position_single_filament,z_position_single_filament,point_number_single_filament = get_point_coordinates_single_filament(x_position, y_position, z_position, filament_number, point_number_filament, number_target)
    x_core, y_core = get_core_coordinates(filename)
    orientation_single_filament = []
    for i in range(len(point_number_single_filament)-1):
        orientation_single_filament.append(orientation(x_position_single_filament[i],y_position_single_filament[i],z_position_single_filament[i],x_position_single_filament[i+1],y_position_single_filament[i+1],z_position_single_filament[i+1]))
    return orientation_single_filament

### get list of filament orientations for a single filament
### Function adapted from 'get_orientation_single_filament' for filaments close to the membrane
def get_orientation_single_filament_membrane(x_position, y_position, z_position, filament_number, point_number_filament, nb_pts_distance_range, filename, number_target):
    x_position_single_filament, y_position_single_filament, z_position_single_filament, point_number_single_filament, nb_pts_distance_range_single_filament = get_point_coordinates_single_filament_membrane(x_position, y_position, z_position, filament_number, point_number_filament, nb_pts_distance_range, number_target)
    x_core, y_core = get_core_coordinates(filename)
    orientation_single_filament = []
    height_single_filament = []
    radial_distance_single_filament = []
    for i in range(len(point_number_single_filament)-1):
        if (nb_pts_distance_range_single_filament[i] != 0) and (nb_pts_distance_range_single_filament[i+1] != 0):
            orientation_single_filament.append(orientation(x_position_single_filament[i],y_position_single_filament[i],z_position_single_filament[i],x_position_single_filament[i+1],y_position_single_filament[i+1],z_position_single_filament[i+1]))
            x_position_center_segment = (x_position_single_filament[i] + x_position_single_filament[i+1])/2
            y_position_center_segment = (y_position_single_filament[i] + y_position_single_filament[i+1])/2
            z_position_center_segment = (z_position_single_filament[i] + z_position_single_filament[i+1])/2
            height_single_filament.append(z_position_center_segment)
            radial_distance_single_filament.append(math.sqrt((x_position_center_segment - x_core)**2 + (y_position_center_segment - y_core)**2))
    return orientation_single_filament, height_single_filament, radial_distance_single_filament

### get list of filament orientations for all filaments
def get_orientation_all_filaments(x_position, y_position, z_position, filament_number, point_number_filament, filename):
    #Initializing the list of lists that will contain all filament orientations, heights and radial distances from the core
    orientation_all_filaments = []
    height_all_filaments = []
    radial_distance_all_filaments = []
    for i in range(int(max(filament_number))):
        orientation_single_filament, height_single_filament, radial_distance_single_filament = get_orientation_single_filament(x_position, y_position, z_position, filament_number, point_number_filament, filename, i)
        orientation_all_filaments.append(orientation_single_filament)
        height_all_filaments.append(height_single_filament)
        radial_distance_all_filaments.append(radial_distance_single_filament)
    return get_joined_list(orientation_all_filaments), get_joined_list(height_all_filaments), get_joined_list(radial_distance_all_filaments)

### Function adapted from 'get_orientation_all_filaments' for filaments close to the membrane
def get_orientation_all_filaments_membrane(x_position, y_position, z_position, filament_number, point_number_filament, nb_pts_distance_range, filename):
    #Initializing the list of lists that will contain all filament orientations, heights and radial distances from the core
    orientation_all_filaments = []
    height_all_filaments = []
    radial_distance_all_filaments = []
    for number_target in range(int(max(filament_number))):
        orientation_single_filament, height_single_filament, radial_distance_single_filament = get_orientation_single_filament_membrane(x_position, y_position, z_position, filament_number, point_number_filament, nb_pts_distance_range, filename, number_target)
        #orientation_single_filament, height_single_filament, radial_distance_single_filament = get_orientation_single_filament(x_position, y_position, z_position, filament_number, point_number_filament, filename, i)
        orientation_all_filaments.append(orientation_single_filament)
        height_all_filaments.append(height_single_filament)
        radial_distance_all_filaments.append(radial_distance_single_filament)
    ## Exctract the lists corresponding to filaments close to membrane (non-empty ones)
    Orientation_filaments_membrane = []
    Height_filaments_membrane = []
    Radial_distance_membrane = []
    for k in range(len(orientation_all_filaments)):
        if (len(orientation_all_filaments[k]) != 0):
            Orientation_filaments_membrane.append(orientation_all_filaments[k])
            Height_filaments_membrane.append(height_all_filaments[k])
            Radial_distance_membrane.append(radial_distance_all_filaments[k])
    return Orientation_filaments_membrane, Height_filaments_membrane, Radial_distance_membrane

def get_list_of_avg_filament_orientations_membrane(Orientation_filaments_membrane):
    ## Get the average orientation for each filament
    AVG_list_orientation_filaments_membrane = []
    for k in range(len(Orientation_filaments_membrane)):
        AVG_list_orientation_filaments_membrane.append(np.average(Orientation_filaments_membrane[k]))
    return AVG_list_orientation_filaments_membrane

### Get the average filament orientation close to the membrane
def get_average_orientation_filaments_membrane(Orientation_filaments_membrane):
    ## Get a list of all average orientations (averaged filament per filament)
    AVG_list_orientation_filaments_membrane = get_list_of_avg_filament_orientations_membrane(Orientation_filaments_membrane)
    ## Get the average of the average filament orientations
    avg_orientation_filament_membrane,std_orientation_filament_membrane = avg_std(AVG_list_orientation_filaments_membrane)
    ## Get the number of filaments close to the membrane
    number_of_filaments_close_to_membrane = len(AVG_list_orientation_filaments_membrane)
    ## Get an estimate of the total polymerization force (in pN)
    polymerization_force = get_polymerization_force(AVG_list_orientation_filaments_membrane,10) #Assuming a polymerization force of 10 pN per filament
    return avg_orientation_filament_membrane, std_orientation_filament_membrane, number_of_filaments_close_to_membrane, polymerization_force

## Subfunction of get_list_of_avg_filament_orientations_membrane_with_selection_radial_distance()
## Inputs:
#1) angle_f_membrane -> list of orientations for one filament, ex: Orientation_filaments_membrane[0] from poda.get_orientation_all_filaments_membrane()
#2) radial_f_membrane _> list of radial distance for one filament, ex: ex: Radial_distance_membrane[0] from poda.get_orientation_all_filaments_membrane()
#3) radius_core = float number used for radial selection, ex: radial_core = 300 nm
def get_average_filament_orientation_membrane_after_selection(angle_f_membrane, radial_f_membrane, radius_core):
    angle_f_membrane_after_selection = []
    for index in range(len(angle_f_membrane)):
        if radial_f_membrane[index] <= radius_core:
            angle_f_membrane_after_selection.append(angle_f_membrane[index])
    if len(angle_f_membrane_after_selection)>0:
        return np.average(angle_f_membrane_after_selection)

## Generalization of get_list_of_avg_filament_orientations_membrane with a selection on radial distance
## Inputs:
#1) Orientation_filaments_membrane = list of lists (from get_orientation_all_filaments_membrane())
#2) Radial_distance_membrane = list of lists (from get_orientation_all_filaments_membrane())
#3) radius_core = float number used for radial selection, ex: radial_core = 300 nm
def get_list_of_avg_filament_orientations_membrane_with_selection_radial_distance(Orientation_filaments_membrane,Radial_distance_membrane,radius_core):
    AVG_list_orientation_filaments_membrane_with_selection = []
    for index in range(len(Orientation_filaments_membrane)):
        avg_orientation_f_membrane = get_average_filament_orientation_membrane_after_selection(Orientation_filaments_membrane[index],Radial_distance_membrane[index],radius_core)
        AVG_list_orientation_filaments_membrane_with_selection.append(avg_orientation_f_membrane)
    ## Extract only float numbers from the list AVG_list_orientation_filaments_membrane
    AVG_list_orientation_filaments_membrane_with_selection_new = []
    for value in AVG_list_orientation_filaments_membrane_with_selection:
        if isinstance(value, np.float64) == True:
            AVG_list_orientation_filaments_membrane_with_selection_new.append(value)
    return AVG_list_orientation_filaments_membrane_with_selection_new

## Generalization of the function get_average_orientation_filaments_membrane(): adding a selection on radial distance
### Get the average filament orientation close to the membrane
def get_average_orientation_filaments_membrane_with_selection_radial_distance(Orientation_filaments_membrane,Radial_distance_membrane,radius_core):
    ## Get a list of all average orientations with selection on radial distance (averaged filament per filament)
    AVG_list_orientation_filaments_membrane_selection_radial_distance = get_list_of_avg_filament_orientations_membrane_with_selection_radial_distance(Orientation_filaments_membrane,Radial_distance_membrane,radius_core)
    ## Get the average of the average filament orientations
    avg_orientation_filament_membrane,std_orientation_filament_membrane = avg_std(AVG_list_orientation_filaments_membrane_selection_radial_distance)
    ## Get the number of filaments close to the membrane
    number_of_filaments_close_to_membrane_selection_radial_distance = len(AVG_list_orientation_filaments_membrane_selection_radial_distance)
    ## Get an estimate of the total polymerization force (in pN)
    polymerization_force_selection_radial_distance = get_polymerization_force(AVG_list_orientation_filaments_membrane_selection_radial_distance,10) #Assuming a polymerization force of 10 pN per filament
    return avg_orientation_filament_membrane, std_orientation_filament_membrane, number_of_filaments_close_to_membrane_selection_radial_distance, polymerization_force_selection_radial_distance

def get_polymerization_force(orientation_list, actin_force):
    inverse_sin_orientation = []
    for orientation in orientation_list:
        if orientation != 0:
            inverse_sin_orientation.append(1/np.sin(orientation*np.pi/180))
    return sum(inverse_sin_orientation)*actin_force


## return a density value (in micro-meter of filament length / micro-meter^3)
## radius, delta and core_h must be in nm
def get_density_interval(filament_number, point_number_filament, filament_length, radial_distance, radius, delta, core_h):
    L_tot = get_total_length(filament_number, point_number_filament, filament_length, radial_distance,radius,delta)
    return 1e6*L_tot/ (np.pi*core_h*((radius +delta)**2 - radius**2))


#sum of filament length inside the interval [radius,radius+delta], in nm
def get_total_length(filament_number, point_number_filament, filament_length, radial_distance,radius,delta):
    filament_length_all_filaments_method_weighted = []
    for number_target in range(int(max(filament_number))):
        point_number_single_filament, filament_length_single_filament,radial_distance_single_filament = get_parameters_for_length(filament_number, point_number_filament, filament_length, radial_distance, number_target)
        length_inside_single_filament = get_length_single_filament(point_number_single_filament, filament_length_single_filament,radial_distance_single_filament,radius,delta)
        if len(length_inside_single_filament)>0:
            filament_length_all_filaments_method_weighted.append(length_inside_single_filament)
    length_all = []
    for i in range(len(filament_length_all_filaments_method_weighted)):
        if len(filament_length_all_filaments_method_weighted[i])>1:
            length_all.append( (len(filament_length_all_filaments_method_weighted[i])-1)*3 )
    return sum(length_all)

def get_density_radius(filament_number, point_number_filament, filament_length, radial_distance, delta, core_h, Radius_lst):
    density_lst = []
    for radius in Radius_lst:
        density_value = get_density_interval(filament_number, point_number_filament, filament_length, radial_distance, radius, delta, core_h)
        density_lst.append(density_value)
    return density_lst

### get list of filament orientations for all filaments inside the interval : radius < radial_distance < radius + delta
def get_orientation_all_filaments_inside_interval(orientation_all_filaments, height_all_filaments, radial_distance_all_filaments,radius,delta):
    orientation_all_filaments_inside_interval = interval(orientation_all_filaments,radial_distance_all_filaments,radius,delta)
    height_all_filaments_inside_interval = interval(height_all_filaments,radial_distance_all_filaments,radius,delta)
    return remove_string_list(orientation_all_filaments_inside_interval), remove_string_list(height_all_filaments_inside_interval)

### Get the list of average filament orientation as a function of height
def get_list_average_filament_orientation_height(filament_orientation,height,bin_width):
    avg_orientation_lst = []
    std_orientation_lst = []
    sem_orientation_lst = []
    len_interval_lst = []
    avg_height_lst = []
    initial_value = min(height)
    while initial_value < max(height):
        avg_orientation, std_orientation, sem_orientation, len_interval = basic_stat(remove_string_list(interval(filament_orientation,height,initial_value,bin_width)))
        avg_orientation_lst.append(avg_orientation)
        std_orientation_lst.append(std_orientation)
        sem_orientation_lst.append(sem_orientation)
        len_interval_lst.append(len_interval)
        avg_height_lst.append(initial_value + bin_width/2)
        initial_value = initial_value + bin_width
    return avg_orientation_lst, std_orientation_lst, sem_orientation_lst, avg_height_lst, len_interval

### Get the list of average filament orientation
def get_list_average_filament_orientation(orientation_all_filaments,height_all_filaments,radial_distance_all_filaments,Radius_lst,delta):
    orientation_lst = [] #List of orientation lists
    avg_orientation_lst = []
    std_orientation_lst = []
    sem_orientation_lst = []
    len_interval_lst = []
    for radius in Radius_lst:
        orientation, height = get_orientation_all_filaments_inside_interval(orientation_all_filaments, height_all_filaments, radial_distance_all_filaments,radius,delta)
        orientation_lst.append(orientation)
        avg_orientation, std_orientation, sem_orientation, len_interval = basic_stat(orientation)
        avg_orientation_lst.append(avg_orientation)
        std_orientation_lst.append(std_orientation)
        sem_orientation_lst.append(sem_orientation)
        len_interval_lst.append(len_interval)
    return orientation_lst, avg_orientation_lst, std_orientation_lst, sem_orientation_lst, len_interval_lst


def get_filament_orientation_core(x_position = None, y_position = None, z_position = None, filament_number = None, point_number_filament = None, filament_length = None, local_energy = None, filename = None, core_radius = None, radius_scale = None ,**kwargs):
    delta_all_filaments, compression_all_filaments, orientation_all_filaments, length_all_filaments, buckling_force_all_filaments, elastic_force_projected_z_all_filaments, elastic_force_all_filaments, elastic_energy_all_filaments, radial_distance_all_filaments = get_elastic_force_and_energy_all_filaments(x_position = x_position, y_position = y_position, z_position = z_position, filament_number = filament_number, point_number_filament = point_number_filament, filament_length = filament_length, local_energy = local_energy, filename = filename)
    filament_orientation_core = remove_string_list(interval(orientation_all_filaments,radial_distance_all_filaments,0,core_radius-radius_scale))
    return {'filament_orientation_core':filament_orientation_core}

def get_filament_orientation_outside(x_position = None, y_position = None, z_position = None, filament_number = None, point_number_filament = None, filament_length = None, local_energy = None, filename = None, core_radius = None, radius_scale = None ,**kwargs):
    delta_all_filaments, compression_all_filaments, orientation_all_filaments, length_all_filaments, buckling_force_all_filaments, elastic_force_projected_z_all_filaments, elastic_force_all_filaments, elastic_energy_all_filaments, radial_distance_all_filaments = get_elastic_force_and_energy_all_filaments(x_position = x_position, y_position = y_position, z_position = z_position, filament_number = filament_number, point_number_filament = point_number_filament, filament_length = filament_length, local_energy = local_energy, filename = filename)
    filament_orientation_outside = remove_string_list(interval(orientation_all_filaments,radial_distance_all_filaments,core_radius+radius_scale,3000))
    return {'filament_orientation_outside':filament_orientation_outside}

def get_filament_length_core(x_position = None, y_position = None, z_position = None, filament_number = None, point_number_filament = None, filament_length = None, local_energy = None, filename = None, core_radius = None, radius_scale = None ,**kwargs):
    delta_all_filaments, compression_all_filaments, orientation_all_filaments, length_all_filaments, buckling_force_all_filaments, elastic_force_projected_z_all_filaments, elastic_force_all_filaments, elastic_energy_all_filaments, radial_distance_all_filaments = get_elastic_force_and_energy_all_filaments(x_position = x_position, y_position = y_position, z_position = z_position, filament_number = filament_number, point_number_filament = point_number_filament, filament_length = filament_length, local_energy = local_energy, filename = filename)
    filament_length_core = remove_string_list(interval(length_all_filaments,radial_distance_all_filaments,0,core_radius-radius_scale))
    return {'filament_length_core':filament_length_core}

def get_filament_length_outside(x_position = None, y_position = None, z_position = None, filament_number = None, point_number_filament = None, filament_length = None, local_energy = None, filename = None, core_radius = None, radius_scale = None ,**kwargs):
    delta_all_filaments, compression_all_filaments, orientation_all_filaments, length_all_filaments, buckling_force_all_filaments, elastic_force_projected_z_all_filaments, elastic_force_all_filaments, elastic_energy_all_filaments, radial_distance_all_filaments = get_elastic_force_and_energy_all_filaments(x_position = x_position, y_position = y_position, z_position = z_position, filament_number = filament_number, point_number_filament = point_number_filament, filament_length = filament_length, local_energy = local_energy, filename = filename)
    filament_length_outside = remove_string_list(interval(length_all_filaments,radial_distance_all_filaments,core_radius+radius_scale,3000))
    return {'filament_length_outside':filament_length_outside}


### join a list of lists
def get_joined_list(list_of_lists):
    joined_list = []
    for i in range(len(list_of_lists)):
        joined_list = [*joined_list, *list_of_lists[i]]
    return joined_list


##########################
### Interval selection ###
##########################

### Flag interval = 1 if initial_value <= parameter < initial_value + delta, flag_interval = 0 otherwise
def flag_interval(parameter,initial_value,delta):
    return [1 if parameter[i]<(initial_value+delta) and parameter[i]>=initial_value else 0 for i in range(len(parameter))]

### Return values of input_variable if inside the interval : initial_value <= parameter < initial_value + delta, returns "Outside" otherwise
def interval(input_variable,parameter,initial_value,delta):
    flag_inside = flag_interval(parameter,initial_value,delta)
    return [input_variable[i] if flag_inside[i]==1 else 'Outside' for i in range(len(input_variable))] ## variable_inside = value if inside interval, string 'Outside' otherwise

### Get rid of strings elements in a list
def remove_string_list(list):
    float_list = []
    for i in range(len(list)):
        if type(list[i]) != str:
            float_list.append(list[i])
    return float_list

#########################
### Statistics ##########
#########################

### Return 2 numbers:(i) average value and tbe standard deviation from a list
def avg_std(lst):
    return np.average(lst), np.std(lst);

### Return the standard error or the mean from a list
def SEM(lst):
    return np.std(lst)/np.sqrt(len(lst)-1) if len(lst)>1 else 0;

### Return 4 numbers:(i) the average value, tbe standard deviation, the standard error of the mean and the length from a list
def basic_stat(lst):
    avg,std = avg_std(lst)
    return avg,std,SEM(lst),len(lst);

#########################
### Density of energy ###
#########################

def get_parameters_for_local_energy(filament_number, radial_distance, local_energy, number_target):
    ## initializing parameters for the 'number target'-th single filament
    radial_distance_single_filament = []
    local_energy_single_filament = []
    for index in range(len(filament_number)):
        if filament_number[index] == number_target:
            radial_distance_single_filament.append(radial_distance[index])
            local_energy_single_filament.append(local_energy[index])
    return radial_distance_single_filament, local_energy_single_filament;

def get_local_energy_single_filament_inside(local_energy_single_filament,radial_distance_single_filament,radius,delta):
    local_energy_sf_inside = interval(local_energy_single_filament,radial_distance_single_filament,radius,delta)
    return remove_string_list(local_energy_sf_inside)

def get_density_energy_all_filaments(filament_number = None, local_energy = None, radial_distance = None, radius = None, delta = None, **kwargs):
    density_energy_all_filaments = []
    energy_all_filaments = []
    for number_target in range(int(max(filament_number))):
        radial_distance_single_filament, local_energy_single_filament = get_parameters_for_local_energy(filament_number, radial_distance, local_energy, number_target)
        local_energy_inside_single_filament = get_local_energy_single_filament_inside(local_energy_single_filament,radial_distance_single_filament,radius,delta)
        if len(local_energy_inside_single_filament)>1:
            density_energy_all_filaments.append(sum(local_energy_inside_single_filament)/((len(local_energy_inside_single_filament)-1)*3))
            energy_all_filaments.append(sum(local_energy_inside_single_filament))
    return density_energy_all_filaments, energy_all_filaments

def get_elastic_energy_core(filament_number = None, local_energy = None, radial_distance = None, radius_core_value = None, **kwargs):
    density_energy_all_filaments_core, energy_all_filaments_core = get_density_energy_all_filaments(filament_number = filament_number, local_energy = local_energy, radial_distance = radial_distance, radius = 0, delta = radius_core_value)
    return sum(energy_all_filaments_core)

def get_density_elastic_energy(filament_number,local_energy,radial_distance,Radius_lst,delta,h_core):
    density_elastic_energy_volume = []
    for index in range(len(Radius_lst)):
        density_energy_all_filaments, energy_all_filaments = get_density_energy_all_filaments(filament_number = filament_number, local_energy = local_energy, radial_distance = radial_distance, radius = Radius_lst[index], delta = delta)
        density_elastic_energy_volume.append(sum(energy_all_filaments)/(np.pi*h_core*( (Radius_lst[index]+delta)**2 - Radius_lst[index]**2 ))) #J/nm^3
    return density_elastic_energy_volume

### Get list of filament length inside the interval : radius < radial_distance < radius + delta
def interval_length(filament_length,radial_distance,radius,delta):
    filament_length_inside_interval = interval(filament_length,radial_distance,radius,delta)
    return np.unique(remove_string_list(filament_length_inside_interval))

### Return 4 lists: (i) average, (ii) tbe standard deviation, (iii) standard error of the mean and (iv) number of points for stat for values of filament length which are inside the interval : radius <= radial < radius + delta
def AVG_length_STD_radius(filament_length,radial,Radius_lst,delta):
    AVG_lst = []
    STD_lst = []
    SEM_lst = []
    len_interval_lst = []
    for i in range(len(Radius_lst)):
        filament_length_interval = interval_length(filament_length,radial,Radius_lst[i],delta)
        avg_value, std_value, sem_value, len_interval = basic_stat(filament_length_interval)
        AVG_lst.append(avg_value)
        STD_lst.append(std_value)
        SEM_lst.append(sem_value)
        len_interval_lst.append(len_interval)
    return AVG_lst, STD_lst, SEM_lst, len_interval_lst;

### Get the length inside interval [radius, radius + delta] from filename
def get_length_inside(filename,radius,delta):
    array = get_data(filename)
    filament_length = get_filament_length(array)
    radial_distance = get_radial_distance(array, filename)
    return interval_length(filament_length,radial_distance,radius,delta)

### Get average filament length inside intervals [Radius, Radius + delta] as a function of Radius (Radius = list of radius values)
def get_AVG_filament_length(filename,Radius_lst,delta):
    array = get_data(filename)
    filament_length = get_filament_length(array)
    radial_distance = get_radial_distance(array,filename)
    AVG_length, STD_length, SEM_lst, len_interval_lst = AVG_length_STD_radius(filament_length,radial_distance,Radius_lst,delta) ## Get the average filament length and std
    return AVG_length, STD_length, SEM_lst, len_interval_lst

###################
### Fits ##########
###################

## Mathematical function with 4 parameters
def fit_func_orientation_radius(x, x_0, a, b, c):
    return 0.5*((a+b) - (b-a)*np.tanh((x-x_0)/c))

def fit_orientation(radius,orientation):
    params = curve_fit(fit_func_orientation_radius, radius, orientation,bounds=([0,0,0,0],[max(radius),10000,10000,10000]))
    [radius_core, param_a, param_b, param_c] = params[0]
    return radius_core, param_a, param_b, param_c

# Mathematical function with 4 parameters
def fit_func_decay(x, x_0, a, b, c):
    return 0.5*((a+b) - (b-a)*np.tanh((x-x_0)/c))

## Mathematical function with 4 parameters
def fit_func_growth(x, x_0, a, b, c):
    return 0.5*((a+b) + (b-a)*np.tanh((x-x_0)/c))

def fit_data_decay(x_data = None, y_data = None, bounds_tuple = None, **kwargs):
    params = curve_fit(fit_func_decay, x_data, y_data, bounds=bounds_tuple)
    [param_x0, param_a, param_b, param_c] = params[0]
    return param_x0, param_a, param_b, param_c

def fit_data_growth(x_data = None, y_data = None, bounds_tuple = None, **kwargs):
    params = curve_fit(fit_func_growth, x_data, y_data, bounds=bounds_tuple)
    [param_x0, param_a, param_b, param_c] = params[0]
    return param_x0, param_a, param_b, param_c

#####################
### Podosome core ###
#####################

def get_total_monomers_core_dict(filament_number = None, point_number_filament = None, filament_length = None, radial_distance = None, core_radius = None, actin_monomer_size = None, **kwargs):
    filament_length_tot = get_total_length(filament_number, point_number_filament, filament_length, radial_distance,0, core_radius) #in nm
    monomers_tot = filament_length_tot / actin_monomer_size
    return {'total_filament_length' : filament_length_tot, 'total_monomers' : monomers_tot}

def get_number_filament_core(radial_distance = None, core_radius = None, filament_number = None, **kwargs):
    return len(np.unique(remove_string_list(interval(filament_number, radial_distance ,0, core_radius))))

def get_average_filament_orientation_core(x_position = None, y_position = None, z_position = None, filament_number = None, point_number_filament = None, filename = None, core_radius = None, **kwargs):
    orientation_all_filaments, height_all_filaments, radial_distance_all_filaments = get_orientation_all_filaments(x_position, y_position, z_position, filament_number, point_number_filament, filename)
    orientation_radius_lst, AVG_orientation, STD_orientation, SEM_orientation, len_interval = get_list_average_filament_orientation(orientation_all_filaments, height_all_filaments, radial_distance_all_filaments,[0], core_radius)
    return AVG_orientation[0]

def get_average_filament_compression_core(x_position = None, y_position = None, z_position = None, filament_number = None, filament_length = None, radial_distance = None, core_radius = None, **kwargs):
    avg_lst_compression,std_lst_compression,sem_lst_compression,len_interval_lst_compression,avg_lst_length,std_lst_length,sem_lst_length,len_interval_lst_length,avg_lst_curvature_predict,std_lst_curvature_predict,sem_lst_curvature_predict,len_interval_lst_curvature_predict = get_average_compression_method_weighted(x_position, y_position, z_position, filament_number, filament_length, radial_distance, [0], core_radius)
    return {'compression_filament_core' : avg_lst_compression[0], 'filament_length_core' : avg_lst_length[0], 'radius_curvature_filament_core': avg_lst_curvature_predict[0]}

def get_average_elastic_energy_per_filament(filament_number = None, energy = None, radial_distance = None, core_radius = None, **kwargs):
    avg_energy_lst,std_energy_lst,sem_energy_lst,len_energy_interval_lst = get_average_energy_method_weighted(filament_number, energy, radial_distance, [0], core_radius)
    return avg_energy_lst[0]

#######################################
### Elastic force and elastic energy###
#######################################

def get_elastic_force(elastic_energy = None, height = None, compression = None, **kwargs):
    return elastic_energy/(height*compression)

def get_buckling_force(number_of_filaments = None, rigidity_actin = None, filament_length = None, **kwargs):
    return number_of_filaments*((np.pi)**2)*rigidity_actin/(filament_length**(2))

def get_parameters_for_elastic_force_and_energy(filament_number, local_energy, x_position, y_position, z_position, filament_length, number_target):
    x_position_single_filament = []
    y_position_single_filament = []
    z_position_single_filament = []
    filament_length_single_filament = []
    local_energy_single_filament = []
    for index in range(len(filament_number)):
        if filament_number[index] == number_target:
            filament_length_single_filament.append(filament_length[index])
            x_position_single_filament.append(x_position[index])
            y_position_single_filament.append(y_position[index])
            z_position_single_filament.append(z_position[index])
            local_energy_single_filament.append(local_energy[index])
    return local_energy_single_filament, x_position_single_filament, y_position_single_filament, z_position_single_filament, filament_length_single_filament;

def get_elastic_force_and_energy_single_filament(x_position = None, y_position = None, z_position = None, filament_number = None, point_number_filament = None, filament_length = None, local_energy = None, filename = None, number_target = None, **kwargs):
    local_energy_single_filament, x_position_single_filament, y_position_single_filament, z_position_single_filament, filament_length_single_filament = get_parameters_for_elastic_force_and_energy(filament_number, local_energy, x_position, y_position, z_position, filament_length, number_target)
    ## Compute filament compression
    x_position_single_filament_1st_point = x_position_single_filament[0]
    x_position_single_filament_last_point = x_position_single_filament[len(x_position_single_filament)-1]
    y_position_single_filament_1st_point = y_position_single_filament[0]
    y_position_single_filament_last_point = y_position_single_filament[len(y_position_single_filament)-1]
    z_position_single_filament_1st_point = z_position_single_filament[0]
    z_position_single_filament_last_point = z_position_single_filament[len(z_position_single_filament)-1]
    chord = get_norm_vector(x_position_single_filament_1st_point,y_position_single_filament_1st_point,z_position_single_filament_1st_point,x_position_single_filament_last_point,y_position_single_filament_last_point,z_position_single_filament_last_point)
    length_filament = filament_length_single_filament[0]
    compression_value_single_filament = 1 - (chord/length_filament)
    ## Get point coordinates of the filament center
    n_points = len(x_position_single_filament)
    if (n_points % 2) == 0:
        index_center_filament = math.ceil(n_points/2) - 1
        x_position_center_filament = (x_position_single_filament[index_center_filament]+x_position_single_filament[index_center_filament+1])/2
        y_position_center_filament = (y_position_single_filament[index_center_filament]+y_position_single_filament[index_center_filament+1])/2
    else:
        index_center_filament = math.ceil(n_points/2) - 1
        x_position_center_filament = x_position_single_filament[index_center_filament]
        y_position_center_filament = y_position_single_filament[index_center_filament]
    ## Get radial distance between the core and the filament center
    x_core, y_core = get_core_coordinates(filename)
    radial_distance_center_filament = (math.sqrt((x_position_center_filament - x_core)**2 + (y_position_center_filament - y_core)**2))
    ## Compute elastic energy
    elastic_energy_single_filament = sum(local_energy_single_filament)
    ## Compute elastic force
    elastic_force_single_filament = get_elastic_force(elastic_energy = elastic_energy_single_filament, height = length_filament*1e-9, compression = compression_value_single_filament)#In Newtons
    ## Compute buckling force
    buckling_force_single_filament = get_buckling_force(number_of_filaments = 1, rigidity_actin = __RIGIDITY_ACTIN_FILAMENT__, filament_length = length_filament*1e-9)
    ##Get Average orientation
    orientation_single_filament = get_orientation_single_filament_simple(x_position, y_position, z_position, filament_number, point_number_filament, filename, number_target)
    avg_orientation_single_filament = np.average(orientation_single_filament)
    ## Compute elastic force projected on z-axis
    elastic_force_single_filament_projected_z_axis = elastic_force_single_filament*np.sin((np.pi/180)*avg_orientation_single_filament)
    return compression_value_single_filament*length_filament, compression_value_single_filament, avg_orientation_single_filament, length_filament, buckling_force_single_filament, elastic_force_single_filament_projected_z_axis, elastic_force_single_filament, elastic_energy_single_filament, radial_distance_center_filament

def get_elastic_force_and_energy_all_filaments(x_position = None, y_position = None, z_position = None, filament_number = None, point_number_filament = None, filament_length = None, local_energy = None, filename = None, **kwargs):
    elastic_energy_all_filaments = []
    elastic_force_all_filaments = []
    elastic_force_projected_z_all_filaments = []
    buckling_force_all_filaments = []
    radial_distance_all_filaments = []
    length_all_filaments = []
    orientation_all_filaments = []
    compression_all_filaments = []
    delta_all_filaments = []
    for number_target in range(int(max(filament_number))):
        delta_length_filament, compression_filament, orientation_filament, length_filament, buckling_force_single_filament, elastic_force_single_filament_projected_z_axis, elastic_force_single_filament, elastic_energy_single_filament, radial_distance_center_filament = get_elastic_force_and_energy_single_filament(x_position = x_position, y_position = y_position, z_position = z_position, filament_number = filament_number, point_number_filament = point_number_filament, filament_length = filament_length, local_energy = local_energy, filename = filename, number_target = number_target)
        buckling_force_all_filaments.append(buckling_force_single_filament)
        elastic_force_all_filaments.append(elastic_force_single_filament)
        elastic_force_projected_z_all_filaments.append(elastic_force_single_filament_projected_z_axis)
        elastic_energy_all_filaments.append(elastic_energy_single_filament)
        radial_distance_all_filaments.append(radial_distance_center_filament)
        length_all_filaments.append(length_filament)
        orientation_all_filaments.append(orientation_filament)
        compression_all_filaments.append(compression_filament)
        delta_all_filaments.append(delta_length_filament)
    return delta_all_filaments, compression_all_filaments, orientation_all_filaments, length_all_filaments, buckling_force_all_filaments, elastic_force_projected_z_all_filaments, elastic_force_all_filaments, elastic_energy_all_filaments, radial_distance_all_filaments

def get_elastic_force_and_energy_core(x_position = None, y_position = None, z_position = None, filament_number = None, point_number_filament = None, filament_length = None, local_energy = None, filename = None, core_radius = None, **kwargs):
    delta_all_filaments, compression_all_filaments, orientation_all_filaments, length_all_filaments, buckling_force_all_filaments, elastic_force_projected_z_all_filaments, elastic_force_all_filaments, elastic_energy_all_filaments, radial_distance_all_filaments = get_elastic_force_and_energy_all_filaments(x_position = x_position, y_position = y_position, z_position = z_position, filament_number = filament_number, point_number_filament = point_number_filament, filament_length = filament_length, local_energy = local_energy, filename = filename)
    elastic_force_projected_z_core = remove_string_list(interval(elastic_force_projected_z_all_filaments,radial_distance_all_filaments,0,core_radius))
    elastic_force_core = remove_string_list(interval(elastic_force_all_filaments,radial_distance_all_filaments,0,core_radius))
    elastic_energy_core = remove_string_list(interval(elastic_energy_all_filaments,radial_distance_all_filaments,0,core_radius))
    buckling_force_core = remove_string_list(interval(buckling_force_all_filaments,radial_distance_all_filaments,0,core_radius))
    return {'total_buckling_force_core':sum(buckling_force_core),'total_elastic_force_projected_z_core':sum(elastic_force_projected_z_core),'total_elastic_force_core':sum(elastic_force_core),'total_elastic_energy_core':sum(elastic_energy_core)}

def get_elastic_forces_list_core(x_position = None, y_position = None, z_position = None, filament_number = None, point_number_filament = None, filament_length = None, local_energy = None, filename = None, core_radius = None, **kwargs):
    delta_all_filaments, compression_all_filaments, orientation_all_filaments, length_all_filaments, buckling_force_all_filaments, elastic_force_projected_z_all_filaments, elastic_force_all_filaments, elastic_energy_all_filaments, radial_distance_all_filaments = get_elastic_force_and_energy_all_filaments(x_position = x_position, y_position = y_position, z_position = z_position, filament_number = filament_number, point_number_filament = point_number_filament, filament_length = filament_length, local_energy = local_energy, filename = filename)
    elastic_force_projected_z_core = remove_string_list(interval(elastic_force_projected_z_all_filaments,radial_distance_all_filaments,0,core_radius))
    elastic_force_core = remove_string_list(interval(elastic_force_all_filaments,radial_distance_all_filaments,0,core_radius))
    buckling_force_core = remove_string_list(interval(buckling_force_all_filaments,radial_distance_all_filaments,0,core_radius))
    length_core = remove_string_list(interval(length_all_filaments,radial_distance_all_filaments,0,core_radius))
    orientation_core = remove_string_list(interval(orientation_all_filaments,radial_distance_all_filaments,0,core_radius))
    energy_core = remove_string_list(interval(elastic_energy_all_filaments,radial_distance_all_filaments,0,core_radius))
    compression_core = remove_string_list(interval(compression_all_filaments,radial_distance_all_filaments,0,core_radius))
    delta_filament_core = remove_string_list(interval(compression_all_filaments,radial_distance_all_filaments,0,core_radius))
    return {'delta_filament_core':delta_filament_core,'compression_core':compression_core,'orientation_core':orientation_core,'energy_core':energy_core,'length_core':length_core,'buckling_force_list_core':buckling_force_core,'elastic_force_projected_z_list_core':elastic_force_projected_z_core,'elastic_force_list_core':elastic_force_core}
